const limitFunctionCallCount=(cb,n)=>{
    let counts=0;
   
        return (...args)=>{
            if (counts < n) {
                counts = counts + 1
                return cb(...args)
            }
            else {
                return null;
            }
    
}
}

module.exports=limitFunctionCallCount