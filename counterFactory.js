const counter=(count)=>{
    if(count===undefined){
        count=1
    }
    return{
        increment(){
            count+=1;
            return count;
        },
        decrement(){
            count-=1;
            return count;
        }

    }
}
module.exports=counter