const cacheFunction=(cb)=>{
    let cache={};
    return (...args)=>{
        let cacheKey = args.reduce((a, b) => a + b, 0)
        if(cacheKey in cache){
            console.log("fetching from cahce at n =",cacheKey);
            return cache[cacheKey]
        }
        else{
            
            let result=cb(...args);
            cache[cacheKey]=result;
            return result;
        }
    }
}

module.exports=cacheFunction;